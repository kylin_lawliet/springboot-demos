package com.blackcat.activity.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Title test
 * @Description 
 * @author zhanghui
 * @date 2020年07月14日 9:50
 * @version V1.0
 * @see 
 * @since V1.0
 */
@RestController
public class TestController {

	@GetMapping(value = "/test")
	String index(){
		return "Hello Spring Boot!";
	}
}

package com.blackcat.activity.service;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Title Activity
 * @Description 
 * @author zhanghui
 * @date 2020年07月14日 11:20
 * @version V1.0
 * @see 
 * @since V1.0
 */
@RestController
@RequestMapping("/activityService")
public interface ActivityService {

	@RequestMapping(value="/startActivityDemo",method= RequestMethod.GET)
	boolean startActivityDemo();
}

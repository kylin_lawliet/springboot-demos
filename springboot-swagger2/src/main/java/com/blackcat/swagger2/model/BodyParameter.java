package com.blackcat.swagger2.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/***
 * <p> 描述 : 参数对象
 * @author : blackcat
 * @date  : 2020/8/27 15:38
*/
@Data
@ApiModel(value = "BodyParameter",description = "body方法体参数")
public class BodyParameter implements Serializable {

	@NotBlank
	@ApiModelProperty(value = "测试id",dataType = "Integer",required = true)
	private Integer id;
	@ApiModelProperty(value = "测试scopeId",dataType = "Integer")
	private Integer scopeId;
	@ApiModelProperty(value = "测试nameGroup",dataType = "Integer")
	private Integer nameGroup;
	@ApiModelProperty(value = "测试type",dataType = "String")
	private String type;
	@ApiModelProperty(value = "测试name",dataType = "String")
	private String name;
	@ApiModelProperty(value = "测试scopeAttribute",dataType = "String")
	private String scopeAttribute;
//	private String[] scope;

}

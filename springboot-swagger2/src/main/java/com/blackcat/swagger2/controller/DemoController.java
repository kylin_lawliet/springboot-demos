package com.blackcat.swagger2.controller;

import com.blackcat.swagger2.model.BodyParameter;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * <p> 描述 : 注解示例
 * @author : blackcat
 * @date  : 2020/8/27 15:17
 *
 * paramType ：查询参数类型，这里有几种形式：
 * 	path	以地址的形式提交数据
 * 	query	直接跟参数完成自动映射赋值
 * 	body	以流的形式提交 仅支持POST
 * 	header	参数在request headers 里边提交
 * 	form	以form表单的形式提交 仅支持POST
*/
@Api(tags="Swagger注解示例Controller")
@RestController
public class DemoController {

	@ApiIgnore() // 用于类或者方法上，可以不被swagger显示在页面上
	@RequestMapping(value = "demoApiIgnore",method = RequestMethod.GET)
	public String demoApiIgnore(){
		return "测试成功！";
	}

	@ApiOperation(value="无参示例", notes="没有参数示例")
	@RequestMapping(value = "demoNoParam", method = RequestMethod.GET)
	public String demoNoParam(){
		return "测试成功！";
	}

	@ApiOperation(value="有参示例", notes="有参数示例-@ApiImplicitParams")
	//@ApiImplicitParam(name="name",value="用户名",dataType="String", paramType = "query")
	@ApiImplicitParams({
			@ApiImplicitParam(name="name",value="姓名",required=true,paramType="form"),
			@ApiImplicitParam(name="age",value="年龄",required=true,paramType="form",dataType="Integer"),
			@ApiImplicitParam(name="username",value="用户名",dataType="string", paramType = "query"),
			@ApiImplicitParam(name="id",value="用户id",dataType="long", paramType = "query")
	})
	@RequestMapping(value = "demoParams",method = RequestMethod.GET)
	public String demoParams(){
		return "测试成功！";
	}

	@ApiOperation(value="有参示例", notes="有参数示例-@ApiParam")
	@RequestMapping(value = "demoParams2",method = RequestMethod.GET)
	public String demoParams2(@ApiParam(name="userId",value="用户id",required=true) @RequestParam String userId){
		return "测试成功！";
	}


	@ApiOperation(value = "一组响应",notes = "一组响应信息")
	@ApiResponses({
			@ApiResponse(code=200,message="请求成功"),
			@ApiResponse(code=400,message="请求参数没填好"),
			@ApiResponse(code=404,message="请求路径没有或页面跳转路径不对")
	})
	@RequestMapping(value = "demoApiResponses",method = RequestMethod.GET)
	public String demoApiResponses(){
		return "测试成功！";
	}

	@ApiOperation("参数对象")
	@RequestMapping(value = "demoParamObject",method = RequestMethod.GET)
	public String demoParamObject(@ModelAttribute BodyParameter param){
		return "测试成功！";
	}

	@ApiOperation("RequestBody对象")
	@RequestMapping(value = "demoBody",method = RequestMethod.POST)
	public String demoBody(@RequestBody BodyParameter param){
		return "测试成功！";
	}

	@ApiOperation(value="对象集合", notes="放入对象集合json串")
	@RequestMapping(value = "/demoList",method = RequestMethod.POST)
	public String demoList(@RequestBody List<BodyParameter> list){
		return "测试成功！";
	}

	@ApiOperation("注解组合")
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="header",name="name",dataType="String",required=true,value="用户的姓名",defaultValue="zhangsan"),
			@ApiImplicitParam(paramType="query",name="pwd",dataType="String",required=true,value="用户的密码",defaultValue="lisi")
	})
	@ApiResponses({
			@ApiResponse(code=400,message="请求参数没填好"),
			@ApiResponse(code=404,message="请求路径没有或页面跳转路径不对")
	})
	@RequestMapping(value="/demoAssociation",method= RequestMethod.GET)
	public String demoAssociation(@RequestHeader("name") String name,@RequestParam("pwd") String pwd) {
		return "测试成功！";
	}

	@ApiOperation("注解组合")
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="header",name="name",dataType="String",required=true,value="用户的姓名",defaultValue="zhangsan"),
			@ApiImplicitParam(paramType="query",name="pwd",dataType="String",required=true,value="用户的密码",defaultValue="lisi")
	})
	@ApiResponses({
			@ApiResponse(code=400,message="请求参数没填好"),
			@ApiResponse(code=404,message="请求路径没有或页面跳转路径不对")
	})
	@RequestMapping(value="/demoAssociation",method= RequestMethod.POST)
	public String demoAssociation2(@RequestBody BodyParameter param,@RequestHeader("name") String name,@RequestParam("pwd") String pwd) {
		return "测试成功！";
	}
}

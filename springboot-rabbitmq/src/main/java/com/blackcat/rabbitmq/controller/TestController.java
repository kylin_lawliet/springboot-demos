package com.blackcat.rabbitmq.controller;

import com.blackcat.rabbitmq.config.Producer;
import com.blackcat.rabbitmq.message.RabbitData;
import com.blackcat.rabbitmq.message.RabbitDataImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述 ：测试
 * @author : zhangdahui
 * @date : 2022/8/15 9:18
 */
@RestController
public class TestController {
    @Autowired
    Producer producer;

    @GetMapping("/producerTest")
    public void producerTest()  {
        // 生产者发送消息
        producer.produce();
    }

    @GetMapping("/userTest")
    public void userTest() {
        // 生产者发送消息
        producer.simple();
    }

    @GetMapping("/workTest")
    public void workTest() {
        // 生产者发送消息
        producer.work();
    }

    @GetMapping("/pushTest")
    public void pushTest() {
        // 生产者发送消息
        producer.push();
    }

    @GetMapping("/directTest")
    public void directTest() {
        // 生产者发送消息
        producer.direct();
    }


    @GetMapping("send")
    public String send() {
        producer.send("delay.mode","", RabbitData.builder().uuid("8888888").message(RabbitDataImpl.builder().data("1234654").build()).build(), 1);
        return "成功";
    }
}

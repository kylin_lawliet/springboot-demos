package com.blackcat.rabbitmq.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author dengsd
 * @Date 2021/11/6 15:18
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RabbitDataImpl implements Serializable {
   private String data;
}

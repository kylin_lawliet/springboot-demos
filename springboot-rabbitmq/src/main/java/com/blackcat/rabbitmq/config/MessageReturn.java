package com.blackcat.rabbitmq.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate.ReturnCallback;
import org.springframework.stereotype.Component;

/**
 * 描述 ：发生异常时的消息返回提醒
 *      需要开启
 *      开启发送失败退回
 *
 * @author : zhangdahui
 * @date : 2022/8/30 10:11
 */

@Slf4j
@Component
public class MessageReturn implements ReturnCallback {
    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        log.info("返回消息回调:{} 应答代码:{} 回复文本:{} 交换器:{} 路由键:{}", message, replyCode, replyText, exchange, routingKey);
    }

}

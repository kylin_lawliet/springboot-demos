package com.blackcat.rabbitmq.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 描述 ：Mq中间件推送转换主体对象
 * @author : zhangdahui
 * @date : 2022/8/18 13:46
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MqPushMainDto  implements Serializable {

    private static final long serialVersionUID = -4343889155281067850L;

    /**
     * 参考代码编号
     */
    private String refCodeNo;
    /**
     * 标准主题
     */
    private String refCodeTopic;
    /**
     * 参考代码中文名称
     */
    private String refCodeCName;
    /**
     * 参考代码英文名称
     */
    private String refCodeEName;
    /**
     * 参考代码定义
     */
    private String refCodeDefined;
    /**
     * 参考代码定义原则
     */
    private String refCodeTenet;
    /**
     * 参考代码参照规范
     */
    private String refCodeNorm;
    /**
     * 参考代码编码规则
     */
    private String refCodeRule;
    /**
     * 参考代码标识格式
     */
    private String refCodeFormat;
    /**
     * 使用系统
     */
    private String useSystem;
    /**
     * 状态  01-新增 02-更新 03-删除 00-未改变
     */
    private String state;
    /**
     * 创建部门
     */
    private String createDept;
    /**
     * 枚举字段集合
     */
    private List<MqPushEnumDto> enums;
}

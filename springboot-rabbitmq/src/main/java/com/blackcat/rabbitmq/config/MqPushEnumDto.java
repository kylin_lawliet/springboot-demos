package com.blackcat.rabbitmq.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 描述 ：Mq中间件推送转换枚举对象
 * @author : zhangdahui
 * @date : 2022/8/18 13:50
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MqPushEnumDto implements Serializable {

    private static final long serialVersionUID = -4351045619539660238L;

    /**
     * 枚举id
     */
    private String enumId;
    /**
     * 枚举值
     */
    private String enumValue;
    /**
     * 枚举含义
     */
    private String enumMean;
    /**
     * 枚举描述
     */
    private String enumDesc;
    /**
     * 状态 01—新增，02—修改，03—删除 00-未改变
     */
    private String state = "00";

    /**
     * 级别
     */
    private String level;
    /**
     * 上级枚举
     */
    private String parentEnum;
    /**
     * 旧枚举值
     */
    private String oldEnumValue;
    /**
     * 使用系统 多个逗号分隔
     */
    private String useSystem;
}

package com.blackcat.rabbitmq.config;

import com.blackcat.rabbitmq.message.RabbitData;
import com.blackcat.rabbitmq.message.RabbitDataImpl;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * 描述 ：动态加载列队测试
 * @author : zhangdahui
 * @date : 2022/8/19 9:50
 */
@Component
public class MqLoadRunner implements ApplicationRunner {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    Producer producer;

    @Override
    public void run(ApplicationArguments args) throws Exception {
//        producer.callback();
//        producer.callback2();
//        producer.callback3();

        producer.refTest();
//        producer.send("aaaa","", RabbitData.builder().uuid("8888888").message(RabbitDataImpl.builder().data("1234654").build()).build(), 1);
//        producer.send("dh-basedata-refDataSync","", RabbitData.builder().uuid("8888888").message(RabbitDataImpl.builder().data("1234654").build()).build(), 1);
//        producer.send("delay.mode","delay.queue", RabbitData.builder().uuid("8888888").message(RabbitDataImpl.builder().data("1234654").build()).build(), 1);
//        System.out.println("加载MQ。。。。");
//        List<ExchangeConfig> exchanges = new ArrayList<>();
//        ExchangeConfig exchangeConfig = new ExchangeConfig();
//        exchangeConfig.setName("testExchange_CUSTOM");
//        exchangeConfig.setType(ExchangeType.CUSTOM);
//        Map<String, Object> arguments = new HashMap<>();
//        arguments.put("x-delayed-type", "direct");
//        exchangeConfig.setArguments(arguments);
//        exchangeConfig.setCustomType("x-delayed-message");
//        exchanges.add(exchangeConfig);
//        ExchangeConfig exchangeConfig2 = new ExchangeConfig();
//        exchangeConfig2.setName("testExchange_FANOUT");
//        exchangeConfig2.setType(ExchangeType.FANOUT);
//        exchanges.add(exchangeConfig2);
//        List<QueueConfig> queues = new ArrayList<>();
//        QueueConfig queueConfig = new QueueConfig();
//        queueConfig.setName("delay.queue");
//        queueConfig.setRoutingKey("delay.queue");
//        queueConfig.setExchangeName(exchangeConfig.getName());
//        queues.add(queueConfig);
//        QueueConfig queueConfig2 = new QueueConfig();
//        queueConfig2.setName("queue.direct.1");
//        queueConfig2.setExchangeName(exchangeConfig2.getName());
//        queues.add(queueConfig2);
//        RabbitMqProperties properties = new RabbitMqProperties();
//        properties.setExchanges(exchanges);
//        properties.setQueues(queues);
//        properties.createExchange();
//        properties.bindingQueueToExchange();
//        Object bean = SpringBeanUtils.getBean(exchangeConfig.getName());
//        Object bean2 = SpringBeanUtils.getBean(queueConfig.getName());
//        System.out.println("bean "+bean);
//        System.out.println("bean2 "+bean2);
//        rabbitTemplate.convertAndSend("fanout.mode","" ,"测试发送消息");
//        rabbitTemplate.convertAndSend("delay.mode","" ,"测试发送消息2");


    }


}

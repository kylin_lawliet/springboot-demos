package com.blackcat.rabbitmq.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate.ConfirmCallback;
import org.springframework.stereotype.Component;

/**
 * 描述 ：消息发送成功的回调
 *  需要开启
 *  开启发送确认
 *
 * @author : zhangdahui
 * @date : 2022/8/30 10:08
 */

@Slf4j
@Component
public class ConfirmReturn implements ConfirmCallback {

    /**
     * 发布者确认的回调。
     *
     * @param correlationData 回调的相关数据。
     * @param ack             ack为真，nack为假
     * @param cause           一个可选的原因，用于nack，如果可用，否则为空。
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        log.info("回调id:" + correlationData.getId());
        if (ack) {
            log.info("消息成功发送到exchange:"+correlationData);
        } else {
            log.error("消息发送exchange失败:" + cause);
        }
    }

}

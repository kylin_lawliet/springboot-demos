package com.blackcat.rabbitmq.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 描述 ：
 *
 * @author : zhangdahui
 * @date : 2022/8/15 9:26
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Simple implements Serializable {

    private String name;
    private String no;
    private int age;
    private String phone;
    private Date createTime;

}

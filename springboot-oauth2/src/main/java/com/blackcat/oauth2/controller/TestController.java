package com.blackcat.oauth2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author blackcat
 * @date 2023/12/26 14:21
 */
@RestController
public class TestController {

    @RequestMapping("/index")
    public String index(){
        return "index";
    }
}

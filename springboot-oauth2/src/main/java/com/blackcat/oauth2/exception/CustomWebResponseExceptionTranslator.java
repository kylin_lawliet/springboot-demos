package com.blackcat.oauth2.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.stereotype.Component;

/**
 * 密码模式错误处理类
 * password模式错误处理，自定义登录失败异常信息
 * @author blackcat
 * @date 2023/12/26 11:21
 */
@Component
public class CustomWebResponseExceptionTranslator implements WebResponseExceptionTranslator {
    @Override
    public ResponseEntity<OAuth2Exception> translate(Exception e) throws Exception {

        OAuth2Exception oAuth2Exception = (OAuth2Exception) e;
        return ResponseEntity
                //.status(oAuth2Exception.getHttpErrorCode())
                .status(200)
                .body(new CustomOauthException(oAuth2Exception.getMessage()));
    }
}

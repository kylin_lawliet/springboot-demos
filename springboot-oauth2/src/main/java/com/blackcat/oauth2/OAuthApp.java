package com.blackcat.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 集成示例
 * @author : zhanghui
 * @date : 2023/12/26 17:21
 * @link 测试接口 https://apifox.com/apidoc/shared-db1b5364-48ad-487a-9dca-d88bafc667b1/api-136675395
 * @link 代码参考文章 https://blog.csdn.net/m0_71817461/article/details/131521086
*/
@SpringBootApplication
public class OAuthApp {
    public static void main(String[] args) {
        SpringApplication.run(OAuthApp.class, args);
    }
}
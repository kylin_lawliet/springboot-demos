package com.blackcat.oauth2.service;

import com.blackcat.oauth2.entity.UserInfo;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @author zhanghui
 * @date 2023/12/26 11:29
 */
@Component
public class UserAuthService implements UserDetailsService {
//    @Autowired
//    private UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        User user = userMapper.getByUsername(username);
//        if(user == null){
//            throw new UsernameNotFoundException("not found the user:"+username);
//        }else{
//            return user;
//        }
        if("admin".equals(username)){
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            return new UserInfo(1, "admin", encoder.encode("123456"));
        }else{
            throw new UsernameNotFoundException("not found the user:"+username);
        }
    }
}

package com.blackcat.oauth2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * @author blackcat
 * @date 2023/12/26 11:30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo implements UserDetails {

    private Integer userId;
    private String userName;
    private String password;

    //将我们的 权限字符串进行分割，并存到集合中，最后供 AuthenticationProvider 使用
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        return null;
    }

    //将我们的 密码 转换成 AuthenticationProvider 能够认识的 password
    @Override
    public String getPassword() {
        return this.password;
    }

    //将我们的 账户 转换成 AuthenticationProvider 能够认识的 username
    @Override
    public String getUsername() {
        return this.userName;
    }

    //都返回 true
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}

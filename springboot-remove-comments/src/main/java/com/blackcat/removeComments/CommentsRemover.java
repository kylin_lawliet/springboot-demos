package com.blackcat.removeComments;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseResult;
import com.github.javaparser.ParserConfiguration;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.comments.BlockComment;
import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.comments.JavadocComment;
import com.github.javaparser.ast.comments.LineComment;
import com.github.javaparser.printer.lexicalpreservation.LexicalPreservingPrinter;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 描述 ：批量删除java注释
 *      LineComment 块注释
 *      BlockComment 块注释
 *      JavadocComment Java文档注释
 * @author : zhangdahui
 * @date : 2022/2/17 15:40
 */
public final class CommentsRemover {
    private CommentsRemover() {}

    public static String doAction(String content) {
        JavaParser javaParser = createJavaParser();
        ParseResult<CompilationUnit> result = javaParser.parse(content);
        Optional<CompilationUnit> optionalCompilationUnit = result.getResult();
        if (!optionalCompilationUnit.isPresent()) {
            return "";
        }
        CompilationUnit compilationUnit = optionalCompilationUnit.get();
        removeComments(compilationUnit);
        return LexicalPreservingPrinter.print(compilationUnit);
    }

    private static void removeComments(CompilationUnit compilationUnit) {
        List<Comment> comments = compilationUnit.getAllContainedComments();
        List<Comment> unwantedComments = comments
                .stream()
                .distinct()
                .filter(CommentsRemover::isValidCommentType)
                .collect(Collectors.toList());
        unwantedComments.forEach(Node::remove);
    }

    /**
     * 创建源码解析器。设置LexicalPreservationEnabled为true，保留源码中的所有语法。
     */
    private static JavaParser createJavaParser() {
        ParserConfiguration parserConfiguration = new ParserConfiguration();
        parserConfiguration.setLexicalPreservationEnabled(true);
        return new JavaParser(parserConfiguration);
    }

    /**
     * 识别注释
     */
    private static boolean isValidCommentType(Comment comment) {
        return comment instanceof LineComment || comment instanceof BlockComment || comment instanceof JavadocComment;
    }
}

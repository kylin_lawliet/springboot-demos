package com.blackcat.removeComments;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * 描述 ：测试
 * @author : zhangdahui
 * @date : 2022/2/17 15:44
 */
public class Test {
    static String path = "D:\\project\\copy";
    static String copyPath = "D:\\project\\copy2";

    public static void main(String[] args) throws IOException {
        // 单个示例
        String fpath = "D:\\project\\blackcat\\springboot-demos\\springboot-remove-comments\\src\\main\\java\\com\\blackcat\\removeComments\\Input.java";
        String content_actual = CommentsRemover.doAction(readFileContent(fpath));
        System.out.println(content_actual);
//        readfile(path); // 整体文件夹
    }

    /**
     * 读取某个文件夹下的所有文件(支持多级文件夹)
     */
    public static boolean readfile(String filepath) throws IOException {
        File file = new File(filepath);
        if (!file.isDirectory()) {
            readFile(file);
        } else if (file.isDirectory()) {
            String[] filelist = file.list();
            for (int i = 0; i < filelist.length; i++) {
                File readfile = new File(filepath + "\\" + filelist[i]);
                if (!readfile.isDirectory()) {
                    readFile(readfile);
                } else if (readfile.isDirectory()) {
                    readfile(filepath + "\\" + filelist[i]);
                }
            }
        }
        return true;
    }

    private static void readFile(File file) throws IOException {
        if (file.getName().contains(".java")) {
            String fileCopyPath = file.getPath().replace(path, copyPath);
            File fileCopy = new File(fileCopyPath);
            File directory = new File(fileCopy.getParent());
            if (!directory.exists()) {// 判断目录是否存在
                System.out.println(" 创建文件夹:" + directory);
                directory.mkdirs();
            }
            System.out.println("```````````"+file.getPath());
            // 去掉注释
            String content = CommentsRemover.doAction(readFileContent(file.getPath()));
            Files.write(Paths.get(fileCopyPath),
                    content.getBytes(StandardCharsets.UTF_8));
        }
    }

    /** 读取文件内容 **/
    private static String readFileContent(String fileName) throws IOException {
        BufferedReader br_input = new BufferedReader(new FileReader(fileName));
        StringBuilder sb = new StringBuilder();
        String content;
        while((content = br_input.readLine()) != null) {
            sb.append(content);
            sb.append('\n');
        }
        return sb.toString();
    }
}


// 不能处理的注释类型

//    /** 配置过滤化参数,不对代码进行格式化 */
//    private static final Document.OutputSettings outputSettings = new Document.OutputSettings().prettyPrint(false);

//    /**
//     * 数组
//     */
//    private final String[] cell = {
//            "1111*",
//            "2222*",
//    };

//    // 123
//    private final String[] cell2 = {
//            "1111*",
//            "2222*",
//    };

//    /** 配置过滤化参数,不对代码进行格式化 */
//    private static final Document.OutputSettings outputSettings = new Document.OutputSettings().prettyPrint(false);

//     @Configuration      //1.主要用于标记配置类，兼备Component的效果。
//     @EnableScheduling   // 2.开启定时任务


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 描述 ：复杂类型下拉框
 *  原理为新建一个隐藏状态的sheet页，用来存储下拉框的值。
 * @author : zhangdahui
 * @date : 2021/7/21 15:38
 */
public class Test2 {

    @Test
     public void test() throws IOException {
         HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
         HSSFSheet sheet = hssfWorkbook.createSheet();
         HSSFRow row = sheet.createRow(0);
         row.createCell(0).setCellValue("名称");
         row.createCell(1).setCellValue("类型");
         String col = "JGLX";   //机构类型
         Map<String, String> boxMap = new HashMap<>();
         boxMap.put("JGLX", "1类型,2类型,3类型");
         int i =0;
         HSSFDataValidation dataValidation = createBox1(hssfWorkbook , col, boxMap , 10 , i , 0);
         if(dataValidation != null) {
                 sheet.addValidationData(dataValidation);
             }
         FileOutputStream out = new FileOutputStream("D:\\download\\browser\\test2.xlsx");
         hssfWorkbook.write(out);
         out.close();
     }
     /**
       *  excel导出，有码值的数据使用下拉框展示。解决下拉框最多255个字符的问题。
       *  原理为新建一个隐藏状态的sheet页，用来存储下拉框的值。
       * @param wb        　　 工作簿  HSSFWorkbook
       * @param col        　　当前列名
       * @param boxMap     　　码值集合
       * @param rows       　　正常sheet页数据，用来指定哪些行需要添加下拉框
       * @param i            　多个码值需要添加下拉，隐藏状态的sheet页名称不能重复，添加i值区分。
       * @param colToIndex    用来指定哪些列需要添加下拉框
       * @return  dataValidation
      */
     public HSSFDataValidation createBox1(HSSFWorkbook wb, String col, Map<String , String> boxMap, int rows, int i, int colToIndex) {
         HSSFDataValidation dataValidation = null;
         String cols = "";
         //查询码值集合,获取当前列的码值。
         if(null != boxMap.get(col)) {
                 cols = boxMap.get(col);
             }
         //新建隐藏状态的sheet，用来存储码值。
         if(cols.length() > 0 && null != cols) {
                 String str[] = cols.split(",");
                 //创建sheet页
                 HSSFSheet sheet = wb.createSheet("hidden"+i);
                //向创建的sheet页添加码值数据。
                 for (int i1 = 0; i1 < str.length; i1++) {
                         HSSFRow row = sheet.createRow(i1);
                         HSSFCell cell = row.createCell((int) 0);
                         cell.setCellValue(str[i1]);
                     }
                 //将码值sheet页做成excel公式
                 Name namedCell = wb.createName();
                 namedCell.setNameName("hidden"+i);
                 namedCell.setRefersToFormula("hidden"+i+"!$A$1:$A$" + str.length);
                 //确定要在哪些单元格生成下拉框
                 DVConstraint dvConstraint = DVConstraint.createFormulaListConstraint("hidden"+i);
                 CellRangeAddressList regions = new CellRangeAddressList(1 , rows , colToIndex , colToIndex);
                 dataValidation = new HSSFDataValidation(regions, dvConstraint);
                 //隐藏码值sheet页
                 int sheetNum = wb.getNumberOfSheets();
                 for(int n=1; n<sheetNum; n++) {
                     wb.setSheetHidden(n, true);
                 }
         }
         return dataValidation;
     }
}

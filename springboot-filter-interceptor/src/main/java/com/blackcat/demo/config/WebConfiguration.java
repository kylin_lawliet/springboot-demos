package com.blackcat.demo.config;

import com.blackcat.demo.interceptor.FirstIndexInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * <p> 描述 ：Web配置
 * @author : blackcat
 * @date : 2020/5/20 14:37
 */
@Slf4j
@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    /**
     * <p> 描述 : 注册拦截器
     * @author : blackcat
     * @date  : 2020/5/20 14:39
     * 在 Spring Boot 中 配置拦截器，只需要实现 WebMvcConfigurer 接口，
     * 在 addInterceptors() 方法中通过 InterceptorRegistry 添加 拦截器 和 匹配路径 即可。
    */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册拦截器
        InterceptorRegistration first = registry.addInterceptor(new FirstIndexInterceptor());
        // 添加拦截请求
        first.addPathPatterns("/index/**");
        // 添加不拦截的请求
        first.excludePathPatterns("/login");

        // 简写格式
//        registry.addInterceptor(new FirstIndexInterceptor()).addPathPatterns("/index/**");
        log.info("注册拦截器");
    }
}

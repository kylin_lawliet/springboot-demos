package com.blackcat.demo.listener;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * <p> 描述 ：监听器
 * @author : blackcat
 * @date : 2020/5/20 13:54
 *
 * Listener 可以监听 web 服务器中某一个 事件操作，并触发注册的 回调函数。
 * 通俗的语言就是在 application，session，request 三个对象 创建/消亡 或者 增删改 属性时，
 * 自动执行代码的功能组件。
 */
@Slf4j
@WebListener // 配置一个ServletContext监听器
public class IndexServletContextListener  implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log.info("开始初始化servlet上下文");
        ServletContext servletContext = sce.getServletContext();
        // 可以全局直接访问。
        servletContext.setAttribute("content", "在servlet上下文中创建的内容");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.info("销毁servlet上下文");
    }
}

package com.blackcat.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * <p> 描述 ：启动程序
 * @author : blackcat
 * @date : 2020/5/20 13:51
 */
@EnableWebMvc // 允许 Spring Boot 配置 Spring MVC 相关自定义的属性，比如：拦截器、资源处理器、消息转换器等。
@ServletComponentScan // 允许 Spring Boot 扫描和装载当前 包路径 和 子路径 下配置的 Servlet。
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}

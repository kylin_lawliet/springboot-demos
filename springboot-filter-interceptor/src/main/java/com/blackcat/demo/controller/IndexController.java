package com.blackcat.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * <p> 描述 ：控制器
 * @author : blackcat
 * @date : 2020/5/20 14:27
 */
@Slf4j
@Controller
@RequestMapping("/index")
public class IndexController {

    /**
     * <p> 描述 :
     * @author : blackcat
     * @date  : 2020/5/22 12:57
     *
     * 测试连接：
     * http://localhost:8003/index/get?filter1=123&filter2=456&interceptor1=789
     * 少一个参数就无法返回 ‘ok’
    */
    @RequestMapping(value="/get")
    @ResponseBody
    public String get() {
        return "ok";
    }
}

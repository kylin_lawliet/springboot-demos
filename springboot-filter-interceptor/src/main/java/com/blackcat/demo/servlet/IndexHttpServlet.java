package com.blackcat.demo.servlet;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.lang.String.format;

/**
 * <p> 描述 ： 配置Servlet
 * @author : blackcat
 * @date : 2020/5/20 14:00
 *
 * Servlet 是一种运行 服务器端 的 java 应用程序，具有 独立于平台和协议 的特性，
 * 并且可以动态的生成 web 页面，它工作在 客户端请求 与 服务器响应 的中间层。
 *
 * 在 Servlet 容器初始化过程中，contextInitialized() 方法会被调用，
 * 在容器销毁时会调用contextDestroyed()。
 */
@WebServlet(name = "IndexHttpServlet",// servlet名称
    displayName = "indexHttpServlet",
    urlPatterns = {"/index/IndexHttpServlet"},
    initParams = {//  初始化参数
        @WebInitParam(name = "param1", value = "123456"),
        @WebInitParam(name = "param2", value = "987654")
    }
)
public class IndexHttpServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.getWriter().println(format("Created by %s", getInitParameter("param1")));
        resp.getWriter().println(format("Created on %s", getInitParameter("createdOn")));
        resp.getWriter().println(format("Servlet context param: %s",
        req.getServletContext().getAttribute("content")));
    }
}

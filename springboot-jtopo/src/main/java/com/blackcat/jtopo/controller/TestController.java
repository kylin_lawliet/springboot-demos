package com.blackcat.jtopo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Title
 * @Description
 * @author zhanghui
 * @date 2020年12月22日 10:49
 * @version V1.0
 * @see
 * @since V1.0
 */
@Controller
public class TestController {

	@GetMapping("index")
	public String toIndex(String name){
		System.out.println(name);
		return "index";
	}

	@GetMapping("index1")
	public String toIndex1(String name){
		System.out.println(name);
		return "index2";
	}
}

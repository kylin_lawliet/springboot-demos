package com.blackcat.jtopo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JTopoApplication {

    public static void main(String[] args) {
        SpringApplication.run(JTopoApplication.class, args);
    }

}

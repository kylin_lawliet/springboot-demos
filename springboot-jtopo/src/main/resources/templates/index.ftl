<!DOCTYPE html>
<html lang="en">
<head>
    <#assign basePath=request.contextPath />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>jtopo</title>
    <script src="https://cdn.bootcss.com/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
    <script src="${basePath}/js/jtopo-0.4.8-min.js" type="text/javascript"></script>
    <script src="http://www.jtopo.com/demo/js/toolbar.js" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            // json数据
            $.getJSON("${basePath}/js/indexJson.json",function (data) {
                var indexJson = data;

                var canvas = document.getElementById('canvas');
                var stage = new JTopo.Stage(canvas);
                //显示工具栏
                showJTopoToobar(stage);
                var scene = new JTopo.Scene();
                stage.add(scene);
                // 设置背景图片
                scene.background = 'http://www.jtopo.com/demo/img/bg.jpg';

                // 中心节点
                var cloudNode = new JTopo.Node(indexJson[0].name);
                cloudNode.setSize(30, 26);// 设置节点大小
                cloudNode.setLocation(360,230);// 设置节点坐标
                cloudNode.layout = {type: 'circle', radius: 150};
                scene.add(cloudNode);

                // 一些节点其他的属性设置
                // 节点字体，例如：treeNode.font = "12px Consolas"
                // 字体颜色，例如：treeNode.fontColor = "255,255,0"
                // 节点文本位置，例如：treeNode.textPosition = "Bottom_Center"
                // 是否显示阴影, 例如:treeNode.shadow = "true"

                // 使用
                for(var i=0; i<indexJson[0].right.length; i++){
                    var treeNode = createNode(scene, indexJson[0].right[i].name);
                    // 创建连接线
                    var link = new JTopo.Link(cloudNode, treeNode);
                    scene.add(link);
                    // 单击事件
                    treeNode.click(function () {
                        alert('click');
                    });
                    // 创建子节点
                    for(var j=0; j<indexJson[0].right[i].fields.length; j++){
                        createSubNode(scene,indexJson[0].right[i].fields[j].name,treeNode);
                    }
                }
                // 引用
                for(var i=0; i<indexJson[0].left.length; i++){
                    var treeNode = createNode(scene, indexJson[0].left[i].name);
                    // 创建连接线
                    var link = new JTopo.Link(cloudNode, treeNode);
                    scene.add(link);
                    // 单击事件
                    treeNode.click(function () {
                        alert('click');
                    });
                    // 创建子节点
                    for(var j=0; j<indexJson[0].left[i].fields.length; j++){
                        createSubNode(scene,indexJson[0].left[i].fields[j].name,treeNode);
                    }
                }
                // 技术属性
                for(var i=0; i<indexJson[0].fields.length; i++){
                    var treeNode = createNode(scene, indexJson[0].fields[i].name);
                    // 创建连接线
                    var link = new JTopo.Link(cloudNode, treeNode);
                    scene.add(link);
                }
                JTopo.layout.layoutNode(scene, cloudNode, true);

                // 鼠标缩放
                scene.addEventListener('mouseup', function(e){
                    if(e.target && e.target.layout){
                        JTopo.layout.layoutNode(scene, e.target, true);
                    }
                });

                // 鼠标单击  这里写绑定事件  所有节点都会有单击事件
                // scene.addEventListener('click', function(e){
                //     alert('click');
                // });
            });
        });

        function createNode(scene,name) {
            var treeNode = new JTopo.CircleNode(name);
            treeNode.fillStyle = '200,255,0';
            treeNode.radius = 15;
            treeNode.setLocation(scene.width * Math.random(), scene.height * Math.random());
            treeNode.layout = {type: 'circle', radius: 80};
            scene.add(treeNode);
            return treeNode;
        }

        function createSubNode(scene,name,treeNode) {
            var vmNode = new JTopo.CircleNode(name);
            vmNode.radius = 10;
            vmNode.fillStyle = '255,255,0';
            vmNode.setLocation(scene.width * Math.random(), scene.height * Math.random());
            scene.add(vmNode);
            scene.add(new JTopo.Link(treeNode, vmNode));
        }

    </script>
</head>
<body>
    <canvas id="canvas" width="800px" height="500px"></canvas>
</body>
</html>

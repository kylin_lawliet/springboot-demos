<!DOCTYPE html>
<html lang="en">
<head>
    <#assign basePath=request.contextPath />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>jtopo</title>
    <script src="https://cdn.bootcss.com/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
    <script src="${basePath}/js/jtopo-0.4.8-min.js" type="text/javascript"></script>
    <script src="http://www.jtopo.com/demo/js/toolbar.js" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            let style = {
                core: 1,
                child: 2,
                leaf: 3,
                field: 4
            };
            // json数据
            $.getJSON("${basePath}/js/indexJson.json",function (result) {
                // var indexJson = result;

                var canvas = document.getElementById('canvas');
                var stage = new JTopo.Stage(canvas);
                //显示工具栏
                showJTopoToobar(stage);
                var scene = new JTopo.Scene();
                stage.add(scene);
                stage.frames = 60;
                // 设置背景图片
                scene.background = 'http://www.jtopo.com/demo/img/bg.jpg';

                // let data = this.data;
                let data = result;
                let style2 = style;

                let coreNode = getNode(data, style2.core);
                showTop(coreNode, data.left, style2.child, style2.core, false,scene);
                let length = Math.max.apply(
                    null,
                    [...data.right, ...data.fields].map(x => x.name.length)
                );
                showTop(coreNode,data.right,style2.child,style2.child,true,true,length,scene);
                showTop(coreNode,data.fields,style2.field,style2.child,true,false,length,scene);
                JTopo.layout.layoutNode(scene, coreNode, true);
                JTopo.layout.layoutNode2(scene, coreNode, true, "left");
                stage.centerAndZoom();
            });
        });


        function getNode(obj, style2, length) {
            let treeNode;
            length = length || obj.name.length;
            treeNode = new JTopo.Node(obj.name);
            if (style2 === style.core) {
                let width = length * 18 + 40;
                treeNode.setSize(width < 128 ? 128 : width, 64);
                treeNode.font = "bold 18px 微软雅黑,黑体";
                treeNode.textOffsetY = -60;
                treeNode.fillColor = "67, 95, 188"; // 填充颜色
                treeNode.fontColor = "255, 255, 255"; // 字体颜色
                treeNode.borderRadius = 5; // 圆角
                treeNode.layout = {
                    type: "tree",
                    direction: "right",
                    width: 80,
                    height: width + 200
                };
                treeNode.addEventListener("mouseup", () => {
                    if (treeNode.layout) {
                        JTopo.layout.layoutNode(this.scene, treeNode, true);
                        JTopo.layout.layoutNode2(this.scene, treeNode, true, "left");
                    }
                });
            } else if (style2 === style.child) {
                let width = length * 16 + 20;
                treeNode.setSize(width, 48);
                treeNode.font = "16px 微软雅黑,黑体";
                treeNode.textOffsetY = -50;
                treeNode.fillColor = "235, 236, 243"; // 填充颜色
                treeNode.fontColor = "48, 48, 48"; // 字体颜色
                treeNode.borderRadius = 5; // 圆角
                treeNode.layout = {
                    type: "tree",
                    direction: "right",
                    width: 30,
                    height: width + 50
                };
                treeNode.addEventListener("mouseup", () => {
                    if (treeNode.layout) {
                        JTopo.layout.layoutNode(this.scene, treeNode, true);
                    }
                });
                treeNode.addEventListener("click", evt => {
                    alert("点击事件");
                });
            } else if (style2 === style.leaf) {
                let width = length * 14 + 10;
                treeNode.setSize(width, 24);
                treeNode.font = "14px 微软雅黑,黑体";
                treeNode.textOffsetY = -34;
                treeNode.fillColor = "255, 255, 255"; // 填充颜色
                treeNode.fontColor = "48, 48, 48"; // 字体颜色
                treeNode.borderWidth = 1; // 边框的宽度
                treeNode.borderColor = "109, 109, 109"; // 边框颜色
                treeNode.borderRadius = 5; // 圆角
            } else if (style === this.style.field) {
                let width = length * 16 + 20;
                treeNode.setSize(width - 10, 38);
                treeNode.font = "16px 微软雅黑,黑体";
                treeNode.textOffsetY = -45;
                treeNode.fillColor = "255, 255, 255"; // 填充颜色
                treeNode.fontColor = "48, 48, 48"; // 字体颜色
                treeNode.borderWidth = 5; // 边框的宽度
                treeNode.borderColor = "235, 236, 243"; // 边框颜色
                treeNode.borderRadius = 5; // 圆角
            }

            scene.add(treeNode);
            return treeNode;
        }
        function getLink(node1, node2, relation, style,scene) {
            let link;

            if (style === this.style.core) {
                link = new JTopo.FlexionalLink(node1, node2, "");
                link.direction = "horizontal";
                link.offsetGap = 0;
                link.lineWidth = 1;
                link.strokeColor = "109, 109, 109";
            } else if (style === this.style.child) {
                link = new JTopo.FlexionalLink(node1, node2, "");
                link.direction = "horizontal";
                link.offsetGap = 0;
                link.lineWidth = 1;
                link.strokeColor = "109, 109, 109";
            } else if (style === this.style.leaf) {
                link = new JTopo.FlexionalLink(node1, node2, "");
                link.direction = "horizontal";
                link.offsetGap = 0;
                link.lineWidth = 1;
                link.strokeColor = "109, 109, 109";
            }

            scene.add(link);
            return link;
        }
        function showTop(core,lst,node_style,link_style,out = true,deep = false,length = null,scene)
        {
            if (!length) {
                length = Math.max.apply(
                    null,
                    lst.map(x => x.name.length)
                );
            }
            let childLength;
            if (deep) {
                let childList = [];
                for (let fieldList of lst.map(x => x.fields)) {
                    childList.push.apply(childList, fieldList);
                }
                childLength = Math.max.apply(
                    null,
                    childList.map(x => x.name.length)
                );
            }
            for (let obj of lst) {
                let node2 = this.getNode(obj, node_style, length);
                if (out) {
                    this.getLink(core, node2, "", link_style);
                } else {
                    this.getLink(node2, core, "", link_style);
                }
                if (deep) {
                    this.showTop(
                        node2,
                        obj.fields,
                        node_style + 1,
                        link_style + 1,
                        out,
                        false,
                        childLength
                    );
                }
            }
        }

    </script>
</head>
<body>
    <canvas id="canvas" width="800px" height="500px"></canvas>
</body>
</html>

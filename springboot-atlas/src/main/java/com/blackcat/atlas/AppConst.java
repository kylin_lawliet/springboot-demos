package com.blackcat.atlas;

/**
 * 描述 ：
 *
 * @author : zhangdahui
 * @date : 2022/2/9 10:19
 */
public class AppConst {

    // 类型名称：表
    public static String TYPE_NAME_TABLE = "hive_table";
    // 类型名称：过程
    public static String TYPE_NAME_PROCESS = "hive_process";
    // 类型名称：字段
    public static String TYPE_NAME_COLUMN = "hive_column";
    // 类型名称：字段血缘
    public static String TYPE_NAME_COLUMN_LINEAGE = "hive_column_lineage";

}

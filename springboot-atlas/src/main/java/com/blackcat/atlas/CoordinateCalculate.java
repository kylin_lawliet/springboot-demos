package com.blackcat.atlas;

import jdk.nashorn.internal.ir.IndexNode;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 描述 ：坐标计算
 * @author : zhangdahui
 * @date : 2022/1/24 14:37
 */
public class CoordinateCalculate {

    //    private static BigDecimal WIDTH = new BigDecimal(100);
    // 节点宽度
    private static Integer WIDTH = 100;
    // 节点右间距
    private static Integer RIGHT_SPACING = 200;
    // 节点下间距
    private static Integer BELOW_SPACING = 20;
    // 列的高度
    private static Integer COLUMN_HEIGHT = 30;
    // 表名的高度
    private static Integer TABLE_HEIGHT = 10;

    /** 初始数据 */
    public static List<IndexNode> LEFT_LIST = new ArrayList<>();
    public static List<IndexNode> RIGHT_LIST = new ArrayList<>();

    public void coordinateStart(){

    }
}

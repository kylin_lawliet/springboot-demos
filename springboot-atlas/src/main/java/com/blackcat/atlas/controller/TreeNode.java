package com.blackcat.atlas.controller;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 描述 ：节点
 * @author : zhangdahui
 * @date : 2022/1/27 17:29
 */
@Data
@NoArgsConstructor
public class TreeNode {
    private String id;
    private String parentId;
    private List<TreeNode> children;

    public TreeNode(String id, String parentId) {
        this.id = id;
        this.parentId = parentId;
    }
}

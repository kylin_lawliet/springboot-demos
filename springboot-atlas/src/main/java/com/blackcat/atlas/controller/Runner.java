package com.blackcat.atlas.controller;

import com.blackcat.atlas.AtlasAPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class Runner implements ApplicationRunner {

	@Autowired
	private AtlasAPIService apiService;

	@Override
	public void run(ApplicationArguments args) {
//		String guid = "e859ef05-0b22-4c3c-b713-ff5091a2306f";
//		apiService.dataConvert();
		System.out.println(apiService.apiSearchDsl());
//		System.out.println(apiService.getLineageData(guid));
//		apiService.getTableInfo()
	}

}

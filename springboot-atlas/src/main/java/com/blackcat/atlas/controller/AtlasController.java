package com.blackcat.atlas.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 描述 ：测试
 * @author : zhangdahui
 * @date : 2022/1/7 10:23
 */
@Controller
public class AtlasController {

    @RequestMapping("/index")
    public String home(){
        return "index";
    }
}

package com.blackcat.atlas;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * 描述 ：节点信息
 * @author : zhangdahui
 * @date : 2022/2/9 9:51
 */
@Data
@NoArgsConstructor
public class Node {
    // 唯一标识
    private String guid;
    // 表/过程 名称
    private String name;
    // 所属数据库
    private String db;
    // 过程SQL
    private String sql;
    // 过程操作类型
    private String operationType;
    // 高度 = 字段节点高度 * 字段个数 + 表名称节点高度
    private BigDecimal width;
    // 宽度 = 字符数 < 3 ? 50 : 字符数 * 12
    private BigDecimal height;
    // 距离左侧坐标
    private BigDecimal left;
    // 距离顶部坐标
    private BigDecimal top;
    // 表字段
    private List<Column> children;
}

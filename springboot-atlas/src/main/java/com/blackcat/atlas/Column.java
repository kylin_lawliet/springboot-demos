package com.blackcat.atlas;

import lombok.Data;

/**
 * 描述 ：字段信息
 * @author : zhangdahui
 * @date : 2022/1/24 16:02
 */
@Data
public class Column {

    // 字段唯一标识
    private String guid;
    // 字段名称
    private String name;
    // 字段类型
    private String type;
    // 字段排序、坐标 0 开始
    private Integer position;
    // 是否高亮 0：是  1：否
    private Integer highlight;

}

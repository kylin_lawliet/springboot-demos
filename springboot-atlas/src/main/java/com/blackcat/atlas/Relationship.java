package com.blackcat.atlas;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 描述 ：关系描述
 * @author : zhangdahui
 * @date : 2022/2/9 9:58
 */
@NoArgsConstructor
@Data
public class Relationship {

    // 连线起始点
    private String start;
    // 连线到达点
    private String end;
    // 起始点字段排序、坐标1开始
    private Integer startPosition;
    // 到达点字段排序、坐标1开始
    private Integer endPosition;
    // 关系连接线颜色
    private String color;
    // 是否高亮 0：是  1：否
    private Integer highlight;

    public Relationship(String start, String end) {
        this.start = start;
        this.end = end;
    }
}

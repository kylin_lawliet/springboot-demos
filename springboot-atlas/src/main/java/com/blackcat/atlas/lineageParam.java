package com.blackcat.atlas;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 描述 ：血缘查询参数
 * @author : zhangdahui
 * @date : 2022/2/9 17:25
 */
@Data
@NoArgsConstructor
public class lineageParam {
    // 唯一标识
    private String guid;
    // 数据深度 默认3
    private Integer depth = 3;
    // 数据类型  0:溯源和影响  1:溯源  2:影响
    private Integer type = 0;
    // 显示类型  0:表加字段  1:只显示表
    private Integer view = 0;
    // 是否显示过程 默认显示
    private Boolean process = true;
    // 是否显示删除数据 默认显示
    private Boolean delete = true;

    public lineageParam(String guid) {
        this.guid = guid;
    }
}

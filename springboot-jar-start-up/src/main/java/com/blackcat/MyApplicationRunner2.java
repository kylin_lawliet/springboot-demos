package com.blackcat;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * @Title
 * @Description
 * @author zhanghui
 * @date 2021年01月26日 9:50
 * @version V1.0
 * @see
 * @since V1.0
 */
@Component
@Order(2)
public class MyApplicationRunner2 implements ApplicationRunner {
	@Override
	public void run(ApplicationArguments args) throws Exception {
		List<String> nonOptionArgs = args.getNonOptionArgs();
		System.out.println("ApplicationRunner2[非选项参数]>>> " + nonOptionArgs);
		Set<String> optionNames = args.getOptionNames();
		for(String optionName: optionNames) {
			System.out.println("ApplicationRunner2[选项参数]>>> name:" + optionName + ";value:" + args.getOptionValues(optionName));
		}
	}
}

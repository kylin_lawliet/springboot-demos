package com.blackcat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


/**
 * @Title @PostConstruct 注解
 * @Description
 * @author zhanghui
 * @date 2021年01月25日 9:46
 * @version V1.0
 * @see
 * @since V1.0
 */
@Component
public class PostConstructExample {

	public PostConstructExample(){
		System.out.println("我最先执行");
	}

	/**
	 *我第二个执行
	 */
//	@Autowired
//	private T t;

	/**
	 *我第三个个执行
	 */
	@PostConstruct
	private void init(){
		System.out.println("第三个个执行");
	}
}

package com.blackcat;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * @Title 此种方式与实现CommandLineRunner接口的区别就是他的参数是ApplicationArguments
 * @Description
 * @author zhanghui
 * @date 2021年01月25日 9:55
 * @version V1.0
 * @see
 * @since V1.0
 */
@Component
@Order(1)
public class MyApplicationRunner implements ApplicationRunner {
	@Override
	public void run(ApplicationArguments args) throws Exception {
		List<String> nonOptionArgs = args.getNonOptionArgs();
		System.out.println("ApplicationRunner1[非选项参数]>>> " + nonOptionArgs);
		Set<String> optionNames = args.getOptionNames();
		for(String optionName: optionNames) {
			System.out.println("ApplicationRunner1[选项参数]>>> name:" + optionName + ";value:" + args.getOptionValues(optionName));
		}
	}
}

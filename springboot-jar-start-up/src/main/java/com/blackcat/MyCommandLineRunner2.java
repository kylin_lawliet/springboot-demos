package com.blackcat;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @Title
 * @Description
 * @author zhanghui
 * @date 2021年01月26日 9:45
 * @version V1.0
 * @see
 * @since V1.0
 */
@Component
@Order(2)
public class MyCommandLineRunner2 implements CommandLineRunner {
	@Override
	public void run(String... args) throws Exception {
		System.out.println("Runner2>>>"+ Arrays.toString(args));
	}
}

package com.blackcat;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @Title 使用CommandLineRunner接口类似于Main方法启动，可以接受一个字符串数组的命令行参数
 * @Description
 * @author zhanghui
 * @date 2021年01月25日 9:53
 * @version V1.0
 * @see
 * @since V1.0
 */
@Component
@Order(1)
public class MyCommandLineRunner implements CommandLineRunner {
	@Override
	public void run(String... args) throws Exception {
		System.out.println("Runner1>>>"+ Arrays.toString(args));
	}
}

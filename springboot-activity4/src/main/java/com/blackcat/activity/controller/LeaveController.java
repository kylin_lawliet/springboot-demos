package com.blackcat.activity.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.blackcat.activity.entity.ActHiActinst;
import com.blackcat.activity.entity.LeaveForm;
import com.blackcat.activity.entity.User;
import com.blackcat.activity.service.IActHiActInstService;
import com.blackcat.activity.service.LeaveService;
import com.blackcat.activity.service.UserService;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.repository.Deployment;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Title 请假
 * @Description 
 * @author zhanghui
 * @date 2020年07月20日 14:18
 * @version V1.0
 * @see 
 * @since V1.0
 */
@Controller
@RequestMapping("leave")
public class LeaveController {

	@Autowired(required = false)
	private LeaveService leaveService;
	@Autowired
	private IActHiActInstService iActHiActInstService;
	@Autowired
	private UserService userService;

	/** 部署流程定义 */
	@ResponseBody
	@RequestMapping("/deployWithClassPath")
	public String deployWithClassPath() {
		// 部署请假流程
		RepositoryService rs = ProcessEngines.getDefaultProcessEngine().getRepositoryService();
		Deployment deployment = rs.createDeployment().addClasspathResource("processes/leave.bpmn20.xml").deploy();
		rs.createProcessDefinitionQuery().deploymentId(deployment.getId()).singleResult();
		return "ok";
	}

	/** 跳转请假管理页面 */
	@RequestMapping("/toLeave")
	public String toLeave(String loginName, ModelMap map){
		QueryWrapper<User> userWrapper = new QueryWrapper<>();
		userWrapper.eq("login_name",loginName);
		User user =userService.getOne(userWrapper);
		map.put("user", user);
		return "/leave/leave";
	}

	/** 请假表单列表数据查询 */
	@ResponseBody
	@RequestMapping("/list")
	public List<LeaveForm> list(String uid){
		QueryWrapper<LeaveForm> queryWrapper = new QueryWrapper();
		queryWrapper.eq("proposer_id", uid).or().eq("agent_id",uid);
		List<LeaveForm> list = leaveService.list(queryWrapper);
		return list;
	}

	/** 添加请假申请表单信息 */
	@ResponseBody
	@RequestMapping("/addApply")
	public String addApply(String uid){
		return leaveService.saveApply(uid);
	}

	/** 经理 提交申请 */
	@ResponseBody
	@RequestMapping("/submitApply")
	public String submitApply(String procInstId){
		System.out.println("经理 提交申请");
		leaveService.submitApply(procInstId);
		return "ok";
	}

	/** 经理 提交申请 */
	@ResponseBody
	@RequestMapping("/bossApply")
	public String bossApply(String procInstId){
		leaveService.bossApply(procInstId);
		return "ok";
	}

	/** 放弃申请，结束流程 */
	@ResponseBody
	@RequestMapping("/giveUp")
	public String giveUp(String procInstId){
		leaveService.giveUp(procInstId);
		return "ok";
	}

	/** 通过实例Id 获取该活动信息 查看过程 */
	@RequestMapping("/HiProcActiList/{procInstId}")
	@ResponseBody
	public List<ActHiActinst> ActiList(@PathVariable("procInstId") String procInstId)
	{
		return iActHiActInstService.selectByByProcInstId(procInstId);
	}

	public static void main(String[] args) {
		// 部署请假流程
		RepositoryService rs = ProcessEngines.getDefaultProcessEngine().getRepositoryService();
		Deployment deploy = rs.createDeployment().addClasspathResource("processes/leave.bpmn").deploy();
		rs.createProcessDefinitionQuery().deploymentId(deploy.getId()).singleResult();
	}
}

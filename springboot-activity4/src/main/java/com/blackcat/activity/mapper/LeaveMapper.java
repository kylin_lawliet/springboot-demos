package com.blackcat.activity.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blackcat.activity.entity.LeaveForm;
import com.blackcat.activity.entity.User;

/**
 * @Title 
 * @Description 
 * @author zhanghui
 * @date 2020年07月20日 13:49
 * @version V1.0
 * @see 
 * @since V1.0
 */
public interface LeaveMapper extends BaseMapper<LeaveForm> {


}

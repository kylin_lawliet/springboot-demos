package com.blackcat.activity.service;

import com.blackcat.activity.entity.ActHiActinst;
import com.blackcat.activity.mapper.ActHiActinstMapper;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.DeploymentBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class ActHiActInstServiceImpl implements IActHiActInstService{

    @Autowired(required = false)
    ActHiActinstMapper actHiActinstMapper;

    @Resource
    private RepositoryService repositoryService;
    @Resource
    private RuntimeService runtimeService;
    @Resource
    private TaskService taskService;

    /**
     *
     * @描述: 通过 实例Id 删除与该实例历史活动相关信息
     *
     * @params:
     * @return:
     * @date: 2018/9/23 20:49
    */
    @Override
    public int deleteByProcInstId(String[] procInstIds)
    {
        return actHiActinstMapper.deleteByProcInstId(procInstIds);
    }

    /**
     *
     * @描述: 通过实例Id 获取该活动信息
     *
     * @params:
     * @return:
     * @date: 2018/9/23 20:50
    */
    @Override
    public List<ActHiActinst> selectByByProcInstId(String procInstId)
    {
        return actHiActinstMapper.selectByByProcInstId(procInstId);
    }
}

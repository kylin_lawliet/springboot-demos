package com.blackcat.activity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blackcat.activity.entity.User;

/**
 * @Title 
 * @Description 
 * @author zhanghui
 * @date 2020年07月20日 14:00
 * @version V1.0
 * @see 
 * @since V1.0
 */
public interface UserService extends IService<User> {
}

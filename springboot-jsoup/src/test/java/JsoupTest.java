import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;

/**
 * @Title 演示示例
 * @Description
 * @author zhanghui
 * @date 2020年12月24日 14:15
 * @version V1.0
 * @see
 * @since V1.0
 */
public class JsoupTest {

	static String url = "https://sgz.ejoy.com/station/ajax/350659087930767364/detail-354732128524580868";
	static Document doc;


	@Test
	void select() {
		// 根据类名称查询
		Elements rows = doc.select(".detail-title");
		Element row = rows.get(0);
		System.out.println(row);

		// 跟上面取到的结果一样
		Element masthead = doc.select(".detail-title").first();
		System.out.println(masthead);
	}

	@Test
	void content(){
		Elements rows = doc.select(".detail-title");
		Element row = rows.get(0);
		System.out.println("取值："+row.text());// 取文本内容
		System.out.println("取值："+row.html());// 取元素内html内容
		System.out.println("取值："+row.outerHtml());// 取整个元素html内容

		System.out.println(row.attr("class"));// 取属性值
	}

	@Test
	void select1(){
		Elements links = doc.select("a[href]"); // 带有href属性的a元素
		Elements pngs = doc.select("img[src$=.png]");// 扩展名为.png的图片
		Elements resultLinks = doc.select("div > img"); //在div元素之后的img元素
		System.out.println("links  "+links);
		System.out.println("pngs  "+pngs);
		System.out.println("resultLinks  "+resultLinks);

		for (Element element : resultLinks) {
			String imgSrc = element.text();
		}
	}

	@Test
	void getElement(){
		System.out.println("getElementById  "+doc.getElementById("wl"));// 通过id来获取
		System.out.println("getElementsByTag  "+doc.getElementsByTag("input"));// 通过标签名字来获取
		System.out.println("getElementsByClass  "+doc.getElementsByClass("dot"));// 通过类名来获取
		System.out.println("getElementsByAttribute  "+doc.getElementsByAttribute("class"));// 通过属性名字来获取
		System.out.println("getElementsByAttributeValue  "+doc.getElementsByAttributeValue("class","dot"));// 通过指定的属性名字，属性值来获取
		System.out.println("getAllElements  "+doc.getAllElements());// 获取所有元素
	}



	@BeforeAll
	static void before() throws IOException {
		doc = Jsoup.connect(url).get();
	}
}

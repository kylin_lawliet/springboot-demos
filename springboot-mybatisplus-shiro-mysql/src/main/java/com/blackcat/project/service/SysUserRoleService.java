package com.blackcat.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blackcat.project.entity.SysUserRole;

/**
 * <p> 用户与角色关系表 服务类
 * @author blackcat
 * @date 2020-02-03
 */
public interface SysUserRoleService extends IService<SysUserRole> {

}

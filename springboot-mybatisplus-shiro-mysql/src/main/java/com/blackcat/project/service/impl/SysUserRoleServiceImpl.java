package com.blackcat.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blackcat.project.entity.SysUserRole;
import com.blackcat.project.mapper.SysUserRoleMapper;
import com.blackcat.project.service.SysUserRoleService;
import org.springframework.stereotype.Service;


/**
 * <p> 用户与角色关系表 服务实现类
 * @author blackcat
 * @date 2020-02-03
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

}

package com.blackcat.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blackcat.project.entity.SysUser;

/**
 * <p> 系统用户表 服务类
 * @author blackcat
 * @date 2020-02-03
 */
public interface SysUserService extends IService<SysUser> {

}

package com.blackcat.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.blackcat.project.entity.SysRoleMenu;
import com.blackcat.project.mapper.SysRoleMenuMapper;
import com.blackcat.project.service.SysRoleMenuService;
import org.springframework.stereotype.Service;


/**
 * <p> 角色与权限关系表 服务实现类
 * @author blackcat
 * @date 2020-02-03
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {

}

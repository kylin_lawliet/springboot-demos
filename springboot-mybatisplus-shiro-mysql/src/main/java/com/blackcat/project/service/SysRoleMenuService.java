package com.blackcat.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.blackcat.project.entity.SysRoleMenu;

/**
 * <p> 角色与权限关系表 服务类
 * @author blackcat
 * @date 2020-02-03
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

}

package com.blackcat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
@ServletComponentScan
public class AspectApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(AspectApplication.class, args);
        System.out.println("启动成功！！！");
    }
}


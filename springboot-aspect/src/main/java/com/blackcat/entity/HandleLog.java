package com.blackcat.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Title
 * @Description
 * @author zhanghui
 * @date 2020年12月07日 17:25
 * @version V1.0
 * @see
 * @since V1.0
 */
@Data
public class HandleLog {

	/**
	 * 访问ip
	 */
	private String ip;
	/**
	 * 操作类型
	 */
	private String operationType;
	/**
	 * 请求参数
	 */
	private String requestParam;

	/**
	 * 响应参数
	 */
	private String responseParam;
	/**
	 * 异常信息
	 */
	private String exception;
	/**
	 * 创建时间
	 */
	private LocalDateTime createTime;
}

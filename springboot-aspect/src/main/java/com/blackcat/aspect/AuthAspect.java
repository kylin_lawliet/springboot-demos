package com.blackcat.aspect;

import com.blackcat.annotation.AuthAnn;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.HttpRequestMethodNotSupportedException;

import java.lang.reflect.Method;

/**
 * <p> 描述 : 权限切面
 * @author : zhangdahui
 * @date  : 2020/12/7 14:03
 *
 * Component：泛指组件
 * Aspect：声明一个切面
 */
@Component
@Aspect
@Slf4j
public class AuthAspect {

    @Before("@annotation(com.blackcat.annotation.AuthAnn)")
    public void doBefore(JoinPoint joinPoint) throws HttpRequestMethodNotSupportedException {
        log.info("进行权限校验");
        AuthAnn authAnn = ((MethodSignature) joinPoint.getSignature()).getMethod().getAnnotation(AuthAnn.class);
        String value = authAnn.value();
        Method method=((MethodSignature) joinPoint.getSignature()).getMethod();
        log.info("方法名称：" + method.getName());
        log.info("方法路径：" + joinPoint.getSignature().getDeclaringTypeName());
        String methodName = joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName();
        if (!"update".equals(value)) {
            throw new HttpRequestMethodNotSupportedException(methodName,"没有权限");
        }
        log.info("权限名称：" + value);
    }
}

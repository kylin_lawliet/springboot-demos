package com.blackcat.annotation;

import java.lang.annotation.*;

/**
 * <p> 描述 : 权限自定义注解
 * @author : zhangdahui
 * @date  : 2020/12/7 14:03
 *
 * Target：使用范围（方法）
 * Retention：注解不仅被保存到class文件中，jvm加载class文件之后，仍然存在
 * Documented：会被 JavaDoc 工具提取成文档
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AuthAnn {

	String value() default "";

}

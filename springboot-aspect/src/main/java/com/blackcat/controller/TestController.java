package com.blackcat.controller;

import com.blackcat.annotation.AuthAnn;
import com.blackcat.entity.HandleLog;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * @Title
 * @Description
 * @author zhanghui
 * @date 2020年12月07日 17:28
 * @version V1.0
 * @see
 * @since V1.0
 */
@RestController
public class TestController {

	@AuthAnn("get")
	@RequestMapping("getMethod")
	public String getMethod1(String id){
		return "getMethod";
	}

	@AuthAnn("update")
	@RequestMapping("updateMethod")
	public String updateMethod(HandleLog log){
		BigDecimal.ONE.divide(BigDecimal.ZERO);
		return "updateMethod";
	}

	@RequestMapping("delMethod")
	public String delMethod(String id){
		return "delMethod";
	}

}

package com.blackcat.activity;

import org.activiti.engine.*;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

/**
 * @Title 申请测试
 * @Description 
 * @author zhanghui
 * @date 2020年07月17日 15:06
 * @version V1.0
 * @see
 * @link https://blog.csdn.net/fly_fly_fly_pig/article/details/81700820 参考该博客
 * @since V1.0
 */
@SpringBootTest
class LeaveProcessTest {

	// 获取默认的流程引擎
	private ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();

	// 实例流程id,用来记录流程,以便获取当前任务
	private String processInstanceId;

	// 部署一个流程
	@Test
	void deploymentProcess(){
		RepositoryService rs = engine.getRepositoryService();
		Deployment deploy = rs.createDeployment().addClasspathResource("processes/LeaveProcess.bpmn").deploy();
		rs.createProcessDefinitionQuery().deploymentId(deploy.getId()).singleResult();
	}

	// 开启流程
	@Test
	void startLeaveProcess(){
		// 获取runtimeservice对象
		RuntimeService runtimeService = engine.getRuntimeService();

		// 根据流程key值,获取流程
		String processKey = "leaveProcess";

		// 将信息加入map,以便传入流程中
		Map<String, Object> variables = new HashMap<>();
		variables.put("employeeName", "张三");
		variables.put("day",10);

		// 开启流程
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processKey, variables);
		// 将得到的实例流程id值赋给之前设置的变量
		processInstanceId = processInstance.getId();
		System.out.println("流程开启成功.......实例流程id:"+processInstanceId);
	}

	// 开始走流程
	// 1.获取当前任务,并且完成
	@Test
	void getTaskAndComplete(){
//		processInstanceId = "12501";
		// 获取taskservice实例
		TaskService taskService = engine.getTaskService();

		// 开始进行流程
		while(engine.getRuntimeService()
				.createProcessInstanceQuery()// 获取查询对象
				.processInstanceId(processInstanceId)// 根据id查询流程实例
				.singleResult()// 获取查询结果,如果为空,说明这个流程已经执行完毕,否则,获取任务并执行
				!=null){
			Task task = taskService.createTaskQuery()// 创建查询对象
					.processInstanceId(processInstanceId)// 通过流程实例id来查询当前任务
					.singleResult();// 获取单个查询结果
			String taskName = task.getName();

			if(taskName.equals("发起申请")){// 职员节点
				completeEmployeeTask(task);
			}else if(taskName.equals("领导审批")){// 领导节点
				completeLeaderTask(task);
			}else{// 经理节点
				completeJingliTask(task);
			}
		}

		System.out.println("审核结束..........");
	}

	// 职员提交申请
	void completeEmployeeTask(Task task){
		// 获取任务id
		String taskId = task.getId();

		// 完成任务
		engine.getTaskService().complete(taskId);
		System.out.println("职员已经提交申请.......");
	}

	// 领导审批
	void completeLeaderTask(Task task){
		// 获取任务id
		String taskId = task.getId();

		// 领导意见
		Map<String, Object> variables = new HashMap<>();
		variables.put("leaderResult", 1);

		// 完成任务
		engine.getTaskService().complete(taskId, variables);
		System.out.println("领导审核完毕........");

	}

	// 经理审批
	void completeJingliTask(Task task){
		// 获取任务id
		String taskId = task.getId();
		String name = task.getName();

		// 经理意见
		// 领导意见和经理意见,用0和1表示,0表示未通过,1表示通过
		// 任一环节设置不同意,陷入死循环
		Map<String, Object> variables = new HashMap<>();
		variables.put("result", 1);

		// 完成任务
		engine.getTaskService().complete(taskId, variables);
		System.out.println("经理审核完毕........,审核经理:"+name);

	}


	/*@Test
	void test(){
		//启动流程服务
		RuntimeService runtimeService = engine.getRuntimeService();
		//启动当前流程
		ProcessInstance pi = runtimeService.startProcessInstanceById(pd.getId());
		//当前流程id
		System.out.println(pi.getId());
		//任务服务
		TaskService taskService = engine.getTaskService();
		//当前流程的任务
		Task task = taskService.createTaskQuery().processInstanceId(pi.getId()).singleResult();
		Map<String,Object> map=new HashMap<>();
		//用来判断当前流程是否通过，流程图中定义的判断条件  flag
		map.put("flag",true);
		//完成当前节点任务，flag值用于进行判断
		taskService.complete(task.getId(),map);
		System.out.println("完成第一个节点任务:任务id"+task.getId()+"____  流程实例ID:"+pi.getId());
		//获取第二个节点信息
		Task task1 = taskService.createTaskQuery().processInstanceId(pi.getId()).singleResult();
		System.out.println("流程实例ID:"+task1.getProcessInstanceId()+"___任务ID:"+task1.getId()+"____任务名称:"+task1.getName());
		Map<String,Object> map1=new HashMap<>();
		//用来判断当前流程是否通过，流程图中定义的判断条件  flag
		map1.put("flag",true);
		//完成当前节点任务，flag值用于进行判断
		taskService.complete(task1.getId(),map1);
		System.out.println("完成第二个节点任务:任务id"+task1.getId()+"____  流程实例ID:"+pi.getId());
		//获取第三个节点信息
		Task task2 = taskService.createTaskQuery().processInstanceId(pi.getId()).singleResult();
		System.out.println("流程实例ID:"+task2.getProcessInstanceId()+"___任务ID:"+task2.getId()+"____任务名称:"+task2.getName());
		taskService.complete(task2.getId());
		System.out.println("完成任务");
	}*/

}

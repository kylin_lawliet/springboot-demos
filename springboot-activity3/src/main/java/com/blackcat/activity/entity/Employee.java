package com.blackcat.activity.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Title 员工
 * @Description 
 * @author zhanghui
 * @date 2020年07月17日 15:04
 * @version V1.0
 * @see 
 * @since V1.0
 */
@Data
public class Employee implements Serializable {

	//职员姓名
	private String employeeName;

	//请假天数
	private int day;

	//请假状态
	private boolean leaveStatus = false;

	//结果状态
	private boolean ResultStatus = false;
}

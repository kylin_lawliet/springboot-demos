package com.blackcat.controller;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import com.blackcat.entity.IndexImport;
import com.blackcat.util.ExportUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * @Title 导入导出测试
 * @Description  https://blog.csdn.net/hxnlyw/article/details/96871319
 * @author zhanghui
 * @date 2020年12月28日 9:42
 * @version V1.0
 * @see
 * @since V1.0
 */
@Controller
public class ImportExportController {

	@RequestMapping("exportExcel")
	public void exportExcel(HttpServletResponse response){
		List<IndexImport> list = new ArrayList<>();
		IndexImport indexImport = new IndexImport();
		indexImport.setAddress("222");
		indexImport.setName("111");
		list.add(indexImport);
		list.add(indexImport);
		list.add(indexImport);
		list.add(indexImport);
		list.add(indexImport);
		ExportUtil.exportSheets(response,list);
	}

	@RequestMapping("toImportExcel")
	public String toImportExcel() throws Exception {
		return "index";
	}

	@ResponseBody
	@RequestMapping("importExcel1")
	public String importExcel(@RequestParam("file") MultipartFile file) throws Exception {
		ImportParams params = new ImportParams();
		params.setTitleRows(0);
		params.setHeadRows(1);
		List<IndexImport> indexS = ExcelImportUtil.importExcel(file.getInputStream(), IndexImport.class, params);
		System.out.println(indexS);
		return "ok";
	}
}

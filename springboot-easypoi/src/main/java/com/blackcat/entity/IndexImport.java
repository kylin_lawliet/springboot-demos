package com.blackcat.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import lombok.Data;

/**
 * @Title 导入测试
 * @Description
 * @author zhanghui
 * @date 2020年12月28日 9:30
 * @version V1.0
 * @see
 * @since V1.0
 */
@Data
@ExcelTarget("indexImport")
public class IndexImport {

	@Excel(name="名称")
	private String name;

	@Excel(name="地址")
	private String address;
}

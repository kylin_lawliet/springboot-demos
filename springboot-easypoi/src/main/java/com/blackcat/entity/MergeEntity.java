package com.blackcat.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelEntity;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 描述 ：合并测试
 * @author : zhangdahui
 * @date : 2021/5/12 10:10
 */
@Data
@NoArgsConstructor
@ExcelTarget("teacherEntity")
public class MergeEntity {

    @Excel(needMerge = true,name = "学生姓名", height = 20, width = 30, isImportField = "true_st")
    private String  name;
    @Excel(needMerge = true,name = "学生性别", replace = { "男_1", "女_2" }, suffix = "生", isImportField = "true_st")
    private int  sex;
    @Excel(needMerge = true,name = "出生日期", databaseFormat = "yyyyMMddHHmmss", format = "yyyy-MM-dd", isImportField = "true_st", width = 20)
    private Date birthday;
    @Excel(needMerge = true,name = "进校日期", databaseFormat = "yyyyMMddHHmmss", format = "yyyy-MM-dd")
    private Date registrationDate;
    @Excel(name = "课程名称", orderNum = "1", width = 25)
    private String  className;
    @Excel(name = "主讲老师", orderNum = "1", width = 25)
    private String mathTeacher;
}
